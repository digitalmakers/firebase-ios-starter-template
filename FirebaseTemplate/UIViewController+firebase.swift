//
//  UIViewController+firebase.swift
//  FirebaseTemplate
//
//  Created by Andrew Morton on 15/7/17.
//  Copyright © 2017 Andrew Morton. All rights reserved.
//

import UIKit
import CoreMedia

extension UIViewController{
    
    
    @objc
    func presentLoginViewController(asLogin:Bool){
        let registerViewController = RegisterViewController()
        registerViewController.displayAsLogin = asLogin
        let navController = UINavigationController(rootViewController: registerViewController)
        navController.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navController.navigationBar.shadowImage = UIImage()
        navController.navigationBar.isTranslucent = true
        navController.navigationBar.barStyle = .blackTranslucent // Required for white statusbar
        self.navigationController?.present(navController, animated: true, completion: nil)
    }
        
    
    
}




extension UIView {
    func layerGradient() {
        let layer : CAGradientLayer = CAGradientLayer()
        layer.frame.size = self.frame.size
        layer.frame.origin = CGPoint(x: 0, y: 0)
        
        let color0 = UIColor(red:0.15, green:0.28, blue:0.75, alpha:1.00).cgColor
        let color1 = UIColor(red:0.28, green:0.68, blue:0.85, alpha:1.00).cgColor
        
        layer.colors = [color0, color1]
        self.layer.insertSublayer(layer, at: 0)
    }
}
