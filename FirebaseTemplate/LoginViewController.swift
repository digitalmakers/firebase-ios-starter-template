//
//  LoginViewController.swift
//  FirebaseTemplate
//
//  Created by Andrew Morton on 15/7/17.
//  Copyright © 2017 Andrew Morton. All rights reserved.
//

import UIKit

class LoginViewController:UIViewController, UITextFieldDelegate, UITextViewDelegate{
    
    let resetPasswordLink = "https://google.com"
    
    let inputContainerView:UIView = {
        let view = UIView()
        //view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 5.0
        view.layer.masksToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    let loginButton:UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor.clear
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 4
        button.clipsToBounds = true
        button.setTitle("Login", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(LoginViewController.handleLogin), for: .touchUpInside)
        return button
    }()
    
    let registerButton:UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor.clear
        button.layer.borderColor = UIColor.white.withAlphaComponent(0.2).cgColor
        button.layer.borderWidth = 1
        button.setTitle("Don't have an account? Register", for: .normal)
        button.backgroundColor = UIColor.white.withAlphaComponent(0.1)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(LoginViewController.handelRegister), for: .touchUpInside)
        return button
    }()
    
    
    let emailTextView:UITextField = {
        let field = AuthTextField()
        field.placeholder = "Email"
        field.keyboardType = .emailAddress
        field.autocapitalizationType = .none
        return field
    }()
    
    let passwordTextView:UITextField = {
        let field = AuthTextField()
        field.placeholder = "Password"
        field.autocapitalizationType = .none
        field.isSecureTextEntry = true
        return field
    }()
    
    let passwordResetLabel:UITextView = {
        let label = UITextView()
        
        let attributedString = NSMutableAttributedString(string: "Trouble logging in? Reset your password")
        attributedString.addAttribute(.font, value:UIFont(name:"Helvetica", size:12.0)!, range:NSMakeRange(0,18))
        attributedString.addAttribute(.font, value:UIFont(name:"Helvetica-Bold", size:12.0)!, range:NSMakeRange(19,20))
        attributedString.addAttribute(.link, value:"http://google.com", range:NSMakeRange(19,20))
        label.attributedText = attributedString
        
        label.textColor = UIColor.white
        label.tintColor = UIColor.white
        label.textAlignment = .center
        label.backgroundColor = UIColor.clear
        
        label.isSelectable = false
        label.isScrollEnabled = false
        label.isUserInteractionEnabled = true
        label.dataDetectorTypes = .link
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    let logo:UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "firebase-logo.png")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let seperator:SeperatorLabel = {
        let sep = SeperatorLabel()
        sep.text = "Or"
        return sep
    }()
    
    
    
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        emailTextView.delegate = self
        passwordTextView.delegate = self
        passwordResetLabel.delegate = self
        
        // Setup UI
        self.view.layerGradient()
        self.view.addSubview(inputContainerView)
        
        self.setupInputFields()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.view.backgroundColor = UIColor.yellow
    }
    
    
    
    // MARK : User Interface
    
    func setupInputFields(){
        
        // Autolayout x, y, width, height
        inputContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        inputContainerView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0).isActive = true
        inputContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        inputContainerView.heightAnchor.constraint(equalToConstant: 100 + 12).isActive = true
        
        // Add inputs
        
        // Email
        inputContainerView.addSubview(emailTextView)
        // Autolayout x, y, width, height
        emailTextView.leftAnchor.constraint(equalTo: inputContainerView.leftAnchor).isActive = true
        emailTextView.topAnchor.constraint(equalTo: inputContainerView.topAnchor).isActive = true
        emailTextView.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
        emailTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        // Password
        inputContainerView.addSubview(passwordTextView)
        // Autolayout x, y, width, height
        passwordTextView.leftAnchor.constraint(equalTo: inputContainerView.leftAnchor).isActive = true
        passwordTextView.topAnchor.constraint(equalTo: emailTextView.bottomAnchor, constant: 12).isActive = true
        passwordTextView.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
        passwordTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        // Login Button
        self.view.addSubview(loginButton)
        
        // Autolayout x, y, width, height
        loginButton.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        loginButton.topAnchor.constraint(equalTo: inputContainerView.bottomAnchor, constant: 12).isActive = true
        loginButton.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        // Login Button
        self.view.addSubview(registerButton)
        // Autolayout x, y, width, height
        registerButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: -1).isActive = true
        registerButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -1).isActive = true
        registerButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: 2    ).isActive = true
        registerButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        // Password Reset
        self.view.addSubview(passwordResetLabel)
        // Autolayout x, y, width, height
        passwordResetLabel.leftAnchor.constraint(equalTo: loginButton.leftAnchor).isActive = true
        passwordResetLabel.topAnchor.constraint(equalTo: loginButton.bottomAnchor, constant: 12).isActive = true
        passwordResetLabel.widthAnchor.constraint(equalTo: loginButton.widthAnchor).isActive = true
        passwordResetLabel.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        
        
        // Logo
        self.view.addSubview(logo)
        // Autolayout x, y, width, height
        logo.centerXAnchor.constraint(equalTo: inputContainerView.centerXAnchor).isActive = true
        logo.bottomAnchor.constraint(equalTo: inputContainerView.topAnchor, constant: -24).isActive = true
        logo.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
        logo.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        // Or sepeartor
        
        //self.view.addSubview(seperator)
        //seperator.centerXAnchor.constraint(equalTo: inputContainerView.centerXAnchor).isActive = true
        //seperator.topAnchor.constraint(equalTo: passwordResetLabel.bottomAnchor, constant: 24).isActive = true
        //seperator.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
        //seperator.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        // Close button
        let closeButton = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(LoginViewController.closeModal))
        closeButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = closeButton
        
    }
    
    @objc
    func handleLogin(){
        
        guard let email = emailTextView.text, email != "", email.isEmail else{
            alert(message:"A valid email is required")
            return
        }
        
        guard let password = passwordTextView.text, password != ""  else{
            alert(message:"Password cannot be blank")
            return
        }
        
        WebService.sharedInstance.login(email: email, password: password) { (user, err) in
            if (err != nil){
                let alert = UIAlertController(title: "Error", message: err?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)            }
            
            else {
                self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @objc func closeModal(){
        self.view.window!.rootViewController?.dismiss(animated: true, completion: nil)
    }
    
    @objc func handelRegister(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func resetPassword(){
        let url = URL(string:self.resetPasswordLink)!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
        
    func alert(message:String){
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: - UITextFieldDelegate
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    // MARK: - UITextViewDelegate
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        print("Link")
        return false
    }
    
    
    
    
    // MARK: - Orientation
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    
    
    
    
    
    
}

