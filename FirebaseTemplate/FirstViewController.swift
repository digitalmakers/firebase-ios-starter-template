//
//  FirstViewController.swift
//  FirebaseTemplate
//
//  Created by Andrew Morton on 15/7/17.
//  Copyright © 2017 Andrew Morton. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        WebService.sharedInstance.getItems()
    }
    
    
    
    
    func addLoginButton(){
        let loginButton = UIBarButtonItem(title: "Login", style: .plain, target: self, action: #selector(FirstViewController.presentLoginViewController))
        self.navigationItem.rightBarButtonItem = loginButton
    }
    func addLogoutButton(){
        let loginButton = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(FirstViewController.logout))
        self.navigationItem.rightBarButtonItem = loginButton
    }
    
    @objc
    func logout(){
        WebService.sharedInstance.logout()
        self.presentLoginViewController(asLogin: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        if WebService.sharedInstance.isAuthenticated {
            addLogoutButton()
        } else {
            addLoginButton()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

