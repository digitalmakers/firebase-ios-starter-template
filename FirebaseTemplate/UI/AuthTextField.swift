//
//  AuthTextField.swift
//  FirebaseTemplate
//
//  Created by Andrew Morton on 16/7/17.
//  Copyright © 2017 Andrew Morton. All rights reserved.
//

import UIKit

class AuthTextField:UITextField{
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = UIColor.white.withAlphaComponent(0.1)
        self.textColor = UIColor.white
        self.tintColor = UIColor.white
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 12, height: 30))
        self.leftView = paddingView
        self.leftViewMode = .always
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override var placeholder: String?{
        get {
            return self.attributedPlaceholder?.string
        }
        set {
            if let newString = newValue {
                let placeholder = NSAttributedString(string: newString, attributes: [.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
                self.attributedPlaceholder = placeholder
            }
            
        }
    }
}
