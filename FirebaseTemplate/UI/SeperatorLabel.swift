//
//  SeperatorLabel.swift
//  FirebaseTemplate
//
//  Created by Andrew Morton on 16/7/17.
//  Copyright © 2017 Andrew Morton. All rights reserved.
//

import UIKit

class SeperatorLabel:UIStackView {
    
    let label = UILabel()
    
    var text:String? {
        get {
            return self.label.text
        }
        set {
            self.label.text = newValue
        }
    }
    
    override init(frame: CGRect) {
        print("Init")
        super.init(frame: frame)
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.label.textAlignment = .center
        self.label.translatesAutoresizingMaskIntoConstraints = false
        self.label.widthAnchor.constraint(equalToConstant: 100)
        
        self.axis = .horizontal
        self.distribution = .fill
        self.alignment = .fill
        
        let line = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 1))
        line.translatesAutoresizingMaskIntoConstraints = false
        line.heightAnchor.constraint(equalToConstant: 3)
        line.backgroundColor = UIColor.red
        
        let line2 = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 1))
        line2.translatesAutoresizingMaskIntoConstraints = false
        line2.heightAnchor.constraint(equalToConstant: 3)
        line2.backgroundColor = UIColor.green
        
        self.addArrangedSubview(line)
        self.addArrangedSubview(self.label)
        self.addArrangedSubview(line2)
        
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    
}
