//
//  UITextField+extension.swift
//  FirebaseTemplate
//
//  Created by Andrew Morton on 15/7/17.
//  Copyright © 2017 Andrew Morton. All rights reserved.
//

import UIKit

extension UITextField{
    
    
    func vaidateEmail(){
        if let email = self.text{
            if email.isEmail{
                clearError()
            } else {
                setError("Not a valid email address")
            }
        }
    }
    
    var isValid:Bool{
        get {
            return self.rightView?.frame.size == CGSize.zero
        }
    }
    
    func validateField()->Bool{
        self .setError("")
        return false
    }
    
    
    
    func setError(_ string:String){
        self.rightView = self.viewForError()
        self.rightViewMode = .always
    }
    
    func viewForError()->UIView{
        let view = UIView(frame: CGRect(x: 0,y: 0,width: self.frame.height, height: self.frame.height))
        view.backgroundColor = UIColor.red
        return view
    }
    
    func clearError(){
        self.rightView = nil
        self.rightViewMode = .never
    }
}
