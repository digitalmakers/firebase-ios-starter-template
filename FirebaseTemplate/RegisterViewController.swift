//
//  LoginViewController.swift
//  FirebaseTemplate
//
//  Created by Andrew Morton on 15/7/17.
//  Copyright © 2017 Andrew Morton. All rights reserved.
//

import UIKit

class RegisterViewController:UIViewController, UITextFieldDelegate{
    
    var displayAsLogin:Bool = false
    
    let inputContainerView:UIView = {
        let view = UIView()
        //view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 5.0
        view.layer.masksToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    let loginRegisterButton:UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor.clear
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 4
        button.clipsToBounds = true
        button.setTitle("Register", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(RegisterViewController.handleRegister), for: .touchUpInside)
        return button
    }()
    
    let loginButton:UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor.clear
        button.layer.borderColor = UIColor.white.withAlphaComponent(0.2).cgColor
        button.layer.borderWidth = 1
        button.setTitle("Already have an account? Login", for: .normal)
        button.backgroundColor = UIColor.white.withAlphaComponent(0.1)
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(RegisterViewController.presentLogin), for: .touchUpInside)
        return button
    }()
    
    let nameTextView:UITextField = {
        let field = AuthTextField()
        field.placeholder = "Name"
        return field
    }()
    
    let emailTextView:UITextField = {
        let field = AuthTextField()
        field.placeholder = "Email"
        field.keyboardType = .emailAddress
        field.autocapitalizationType = .none
        return field
    }()
    
    let passwordTextView:UITextField = {
        let field = AuthTextField()
        field.placeholder = "Password"
        field.autocapitalizationType = .none
        field.isSecureTextEntry = true
        return field
    }()
    
    let logo:UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "firebase-logo.png")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let benefitsLabel:UILabel = {
        let label = UILabel()
        label.numberOfLines = 5
        label.text = "Add some descriptve text to explain why you would want to register for an account"
        label.font = UIFont.systemFont(ofSize: 14)
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    
    
    
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        nameTextView.delegate = self
        emailTextView.delegate = self
        passwordTextView.delegate = self
        
        // Setup UI
        self.view.layerGradient()
        self.view.addSubview(inputContainerView)
        
        self.setupInputFields()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.view.backgroundColor = UIColor.yellow
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        if self.displayAsLogin == true {
            self.displayAsLogin = false
            let loginVc = LoginViewController()
            self.navigationController?.pushViewController(loginVc, animated: false)
        }
    }
    
    // MARK : User Interface
    
    func setupInputFields(){
        
        // Autolayout x, y, width, height
        inputContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        inputContainerView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0).isActive = true
        inputContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
        inputContainerView.heightAnchor.constraint(equalToConstant: 150 + 24).isActive = true
        
        // Add inputs
        
        // Name
        inputContainerView.addSubview(nameTextView)
        // Autolayout x, y, width, height
        nameTextView.leftAnchor.constraint(equalTo: inputContainerView.leftAnchor).isActive = true
        nameTextView.topAnchor.constraint(equalTo: inputContainerView.topAnchor).isActive = true
        nameTextView.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
        nameTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        // Email
        inputContainerView.addSubview(emailTextView)
        // Autolayout x, y, width, height
        emailTextView.leftAnchor.constraint(equalTo: inputContainerView.leftAnchor).isActive = true
        emailTextView.topAnchor.constraint(equalTo: nameTextView.bottomAnchor, constant: 12).isActive = true
        emailTextView.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
        emailTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        // Password
        inputContainerView.addSubview(passwordTextView)
        // Autolayout x, y, width, height
        passwordTextView.leftAnchor.constraint(equalTo: inputContainerView.leftAnchor).isActive = true
        passwordTextView.topAnchor.constraint(equalTo: emailTextView.bottomAnchor, constant: 12).isActive = true
        passwordTextView.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
        passwordTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        // Login Button
        self.view.addSubview(loginRegisterButton)
        
        // Autolayout x, y, width, height
        loginRegisterButton.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        loginRegisterButton.topAnchor.constraint(equalTo: inputContainerView.bottomAnchor, constant: 12).isActive = true
        loginRegisterButton.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
        loginRegisterButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        // Login Button
        self.view.addSubview(loginButton)
        // Autolayout x, y, width, height
        loginButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: -1).isActive = true
        loginButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -1).isActive = true
        loginButton.widthAnchor.constraint(equalTo: view.widthAnchor, constant: 2).isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        // Benefits Label
        self.view.addSubview(benefitsLabel)
        // Autolayout x, y, width
        benefitsLabel.centerXAnchor.constraint(equalTo: inputContainerView.centerXAnchor).isActive = true
        benefitsLabel.bottomAnchor.constraint(equalTo: inputContainerView.topAnchor, constant: -24).isActive = true
        benefitsLabel.widthAnchor.constraint(lessThanOrEqualToConstant: 280).isActive = true
        benefitsLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 20).isActive = true
        
        // Logo
        self.view.addSubview(logo)
        // Autolayout x, y, width, height
        logo.centerXAnchor.constraint(equalTo: inputContainerView.centerXAnchor).isActive = true
        logo.bottomAnchor.constraint(equalTo: benefitsLabel.topAnchor, constant: -24).isActive = true
        logo.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
        logo.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        // Close button
        let closeButton = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(RegisterViewController.closeModal))
        closeButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = closeButton
    }
    
    @objc
    func handleRegister(){
        
        guard let name = nameTextView.text, name != "" else{
            alert(message:"A valid name is required")
            return
        }
        
        guard let email = emailTextView.text, email != "", email.isEmail else{
            alert(message:"A valid email is required")
            return
        }
        
        guard let password = passwordTextView.text, password.characters.count > 5  else{
            alert(message:"Password needs to be 6 or more charactures long")
            return
        }
        
        WebService.sharedInstance.registerUser(email: email, password: password) { (user, err) in
            
            if (err != nil){
                let alert = UIAlertController(title: "Error", message: err?.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
            guard let uid = user?.uid else {
                return
            }
            
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    @objc
    func presentLogin(){
        let loginVc = LoginViewController()
        self.navigationController?.pushViewController(loginVc, animated: true)
    }
    
    @objc
    func closeModal(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func alert(message:String){
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    // MARK: - UITextFieldDelegate
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    
    // MARK: - Orientation
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    
    
    
}
