//
//  WebService.swift
//  FirebaseTemplate
//
//  Created by Andrew Morton on 15/7/17.
//  Copyright © 2017 Andrew Morton. All rights reserved.
//

import Foundation
import Firebase


class WebService:NSObject{
    
   let ref = FIRDatabase.database().reference()
    
    var isAuthenticated:Bool {
        return FIRAuth.auth()?.currentUser?.uid != nil
    }
    
    func logout(){
        do {
           try FIRAuth.auth()?.signOut()
        } catch {
            print("Error logging out")
        }
    }
    
    func registerUser(email:String, password:String, callback: @escaping ((FIRUser?, Error?)->())){
        FIRAuth.auth()?.createUser(withEmail: email, password: password, completion: { (user, error) in
            callback(user, error)
        })
    }
    
    func login(email:String, password:String, callback: @escaping ((FIRUser?, Error?)->())){
        FIRAuth.auth()?.signIn(withEmail: email, password: password, completion: { (user, error) in
            callback(user, error)
        })
    }
    
    
    
    // MARK: - Database
    
    func getItems(){
        ref.child("Items").observe(.value) { (snapshot) in
            print("snapstop")
            print(snapshot)
        }
    }
    
    
        
    
    
    class var sharedInstance: WebService {
        struct Static {
            static let instance: WebService = WebService()
        }
        return Static.instance
    }
    
}
