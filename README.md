
# FIREBASE Starter Template

This project is a starting point for iOS Firebase projects. The template assumes that you want typical user features including registration and authentication via email.

Login screens use Autolayout and are compleatly created in code so they should be transportable between projects. 

## Setup

1. Create a new project [https://console.firebase.google.com](https://console.firebase.google.com) and enable **Email authentication**

2. Update Cocopods with `pod update`

3. Download your `GoogleService-Info.plist` and add it to your project

4. Build and run

## Included features

- User registration and authentication
- Helper functions for login/logout flow

## UIViewController extensions

### Present the login modal

Creates a modal view for logging in and/or registration. If `asLogin:` is true then the login view will appear first. Useful when someone has just logged out for example.

`UIViewController.presentLoginViewController(asLogin:Bool)`

### Logout

Logs out a user and presents a modal to login again

`UIViewController.logout()`


### Backlog

- Facebook login
- Twitter login
- Add Firebase functions templates
